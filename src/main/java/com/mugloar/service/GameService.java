package com.mugloar.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.mugloar.model.Dragon;
import com.mugloar.resource.BattleResource;
import com.mugloar.resource.GameResource;
import com.mugloar.resource.ReportResource;

@Service
public class GameService {
	
	@Autowired
	RestTemplate restTemplate;
	
	@Value("${opponent.host}")
	String host;
	
	public GameResource createGame(){
		
		GameResource newGame = restTemplate.getForObject(host + "/api/game", GameResource.class);		
		return newGame;
	}
	
	public BattleResource doBattle(Dragon dragon,Long gameId){
		HttpHeaders headers = new HttpHeaders(); 
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Dragon> dragonEntity = new HttpEntity<Dragon>(dragon,headers);
		ResponseEntity<BattleResource> response = restTemplate.exchange(host + "/api/game/" + gameId.toString() + "/solution", 
														HttpMethod.PUT, dragonEntity, BattleResource.class);	
		return response.getBody();
	}
	
	public ReportResource getWeather(Long gameId){		
		
		ReportResource resultXML = restTemplate.getForObject(host + "/weather/api/report/" + gameId.toString(), ReportResource.class);
		return resultXML;
		
		
	}
	
}
