package com.mugloar.controller;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import static java.util.stream.Collectors.toMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mugloar.model.Dragon;
import com.mugloar.resource.BattleResource;
import com.mugloar.resource.DragonResource;
import com.mugloar.resource.GameResource;
import com.mugloar.resource.KnightResource;
import com.mugloar.resource.ReportResource;
import com.mugloar.service.GameService;

@RestController
@RequestMapping(value="/rest/mugloar")
public class GameController {

	@Autowired
	GameService gameProxy;
	
	@RequestMapping(value="/create", method=RequestMethod.GET)
	public ResponseEntity<GameResource> createNewGame(){		
		ResponseEntity<GameResource> gRes = new ResponseEntity<GameResource>(gameProxy.createGame(),HttpStatus.OK);
		return gRes;
	}
	
	@RequestMapping(value="/weather", method=RequestMethod.GET)
	public ResponseEntity<ReportResource> getWeatherReport(@RequestParam("gameId") Long gameId){
		ResponseEntity<ReportResource> repRes = new ResponseEntity<ReportResource>(gameProxy.getWeather(gameId),HttpStatus.OK);
		return repRes;
	}
	
	@RequestMapping(value="/battle", method=RequestMethod.POST)
	public ResponseEntity<BattleResource> doBattle(@RequestBody DragonResource dragonResource,@RequestParam("gameId") Long gameId){	
		Dragon dragon = new Dragon();
		dragon.setScaleThickness(dragonResource.getScaleThickness());
		dragon.setClawSharpness(dragonResource.getClawSharpness());
		dragon.setWingStrength(dragonResource.getWingStrength());
		dragon.setFireBreath(dragonResource.getFireBreath());		

		ResponseEntity<BattleResource> batRes = new ResponseEntity<BattleResource>(gameProxy.doBattle(dragon, gameId),HttpStatus.OK);
		return batRes;
	}
	
	@RequestMapping(value="/dragon", method=RequestMethod.POST)
	public DragonResource getDragon(@RequestBody KnightResource knightResource, @RequestParam("weatherCode")  String weatherCode){
		int scaleThickness = 0,clawSharpness = 0,wingStrength = 0,fireBreath = 0,start=1,divide=2;
		int attack = knightResource.getAttack(), armor = knightResource.getArmor(), agility = knightResource.getAgility(), endurance = knightResource.getEndurance();
		
		switch(weatherCode){
			case "NMR":		
			    Map<String, Integer> knightMap = new HashMap<>(4);
				knightMap.put("scaleThickness", attack);
				knightMap.put("clawSharpness", armor);
				knightMap.put("wingStrength", agility);
				knightMap.put("fireBreath", endurance);
				
				Map<String, Integer> modKnightMap = knightMap.entrySet()
													.stream()
													.filter(e -> e.getValue() > 0)
													.sorted((e1, e2) -> e1.getValue().compareTo(e2.getValue()))
													.collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

				for (Map.Entry<String, Integer> e : modKnightMap.entrySet()) {
					int value = e.getValue();
					String ability = e.getKey();

					if (start == modKnightMap.size()) {
						knightMap.put(ability, value + 2);
						break;

					} else {
						if (divide > 0) {
							knightMap.put(ability, value - 1);
							divide--;
						}
					}

					start++;
				}
				
				scaleThickness = knightMap.get("scaleThickness");
				clawSharpness = knightMap.get("clawSharpness");
				wingStrength = knightMap.get("wingStrength");
				fireBreath = knightMap.get("fireBreath");
				
				break;
			case "SRO":
				scaleThickness = 5;
				clawSharpness = 5;
				wingStrength = 5;
				fireBreath = 5;
				break;
			case "HVA":
				scaleThickness = 0;
				clawSharpness = 10;
				wingStrength = 10;
				fireBreath = 0;
				break;
			case "T E":
				scaleThickness = 5;
				clawSharpness = 5;
				wingStrength = 5;
				fireBreath = 5;
				break;
			case "FUNDEFINEDG":
				scaleThickness = attack;
				clawSharpness = armor;
				wingStrength = agility;
				fireBreath = endurance;
				break;
			
		}
				
		DragonResource dgRes = new DragonResource();
		dgRes.setScaleThickness(scaleThickness);
		dgRes.setClawSharpness(clawSharpness);
		dgRes.setWingStrength(wingStrength);
		dgRes.setFireBreath(fireBreath);		

		return dgRes;
	}
	
	
}
