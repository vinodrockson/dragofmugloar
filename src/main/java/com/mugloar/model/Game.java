package com.mugloar.model;

public class Game {
	
	Long gameId;
	Knight knight;
	
	public Long getGameId() {
		return gameId;
	}
	public void setGameId(Long gameId) {
		this.gameId = gameId;
	}
	public Knight getKnight() {
		return knight;
	}
	public void setKnight(Knight knight) {
		this.knight = knight;
	}
}
