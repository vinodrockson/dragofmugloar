package com.mugloar.model;

public class Dragon {
	
	int scaleThickness;
	int clawSharpness;
	int wingStrength;
	int fireBreath;
	
	public int getScaleThickness() {
		return scaleThickness;
	}
	public void setScaleThickness(int scaleThickness) {
		this.scaleThickness = scaleThickness;
	}
	public int getClawSharpness() {
		return clawSharpness;
	}
	public void setClawSharpness(int clawSharpness) {
		this.clawSharpness = clawSharpness;
	}
	public int getWingStrength() {
		return wingStrength;
	}
	public void setWingStrength(int wingStrength) {
		this.wingStrength = wingStrength;
	}
	public int getFireBreath() {
		return fireBreath;
	}
	public void setFireBreath(int fireBreath) {
		this.fireBreath = fireBreath;
	}
	
}
