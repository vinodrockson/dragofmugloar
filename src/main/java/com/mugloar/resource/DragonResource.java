package com.mugloar.resource;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DragonResource extends ResourceSupport{
	
	int scaleThickness;
	int clawSharpness;
	int wingStrength;
	int fireBreath;
	
	public int getScaleThickness() {
		return scaleThickness;
	}
	public void setScaleThickness(int scaleThickness) {
		this.scaleThickness = scaleThickness;
	}
	public int getClawSharpness() {
		return clawSharpness;
	}
	public void setClawSharpness(int clawSharpness) {
		this.clawSharpness = clawSharpness;
	}
	public int getWingStrength() {
		return wingStrength;
	}
	public void setWingStrength(int wingStrength) {
		this.wingStrength = wingStrength;
	}
	public int getFireBreath() {
		return fireBreath;
	}
	public void setFireBreath(int fireBreath) {
		this.fireBreath = fireBreath;
	}
}
