package com.mugloar.resource;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class GameResource extends ResourceSupport {
	
	Long gameId;
	KnightResource knight;
	
	public KnightResource getKnight() {
		return knight;
	}
	public void setKnight(KnightResource knight) {
		this.knight = knight;
	}
	public Long getGameId() {
		return gameId;
	}
	public void setGameId(Long gameId) {
		this.gameId = gameId;
	}


}
