var app = angular.module('project', ['ngRoute']);

app.config(function($routeProvider) {
	$routeProvider
	    .when('/', {
	      controller: 'GameController',
	      templateUrl: 'views/main.html'
	    })
	    .when('/battle', {
	      controller: 'BattleController',
	      templateUrl: 'views/battle.html'
	    })
	    .otherwise({
	      redirectTo:'/'
	    });
	});