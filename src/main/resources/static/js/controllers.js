var app = angular.module('project');

app.controller('GameController', function($scope, $http, $route, $location) {

	$scope.startTournament = function () {
		var totalBattles = $scope.totbat;
		$location.path('/battle').search({fights : totalBattles}); 			
	}
	
});

app.controller('BattleController', function($scope, $http, $route,$location,$routeParams,$timeout) {
	$scope.knights = [];
	$scope.knightImgs1 = [];
	$scope.knightImgs2 = [];
	$scope.dragons = [];
	$scope.dragonImgs = [];
	$scope.weathers = [];	
	$scope.games = [];
	$scope.results = [];
	
	var totalBattles = $routeParams.fights, knightPoints = 0, dragonPoints = 0;	

	for(var battle=1;battle<=totalBattles;battle++){	
		$http.get('/rest/mugloar/create').success(function(data, status, headers, config) {  
			$scope.newGame = data;
			$scope.games.push($scope.newGame);
			var knightName = $scope.newGame.knight.name;
			if($scope.knights.indexOf(knightName) == -1){
				$scope.knights.push(knightName);
				$scope.knightImgs1.push(getRandomImage("knights"));
			}
			$scope.knightImgs2.push($scope.knightImgs1[$scope.knights.indexOf(knightName)]);
			$http.get('/rest/mugloar/weather', {params: { gameId: $scope.newGame.gameId}}).success(function(data, status, headers, config) {
				$scope.weatherReport = data;
				$scope.weathers.push($scope.weatherReport);
				$http.post('/rest/mugloar/dragon', $scope.newGame.knight, {params: {weatherCode: $scope.weatherReport.code}}).success(function(data, status, headers, config) {
					$scope.dragon = data;
					$scope.dragons.push($scope.dragon);
					$scope.dragonImgs.push(getRandomImage("dragons"));
					$http.post('/rest/mugloar/battle', $scope.dragon, {params: { gameId: $scope.newGame.gameId}}).success(function(data, status, headers, config) {
						$scope.battleResult = data;	
						$scope.results.push($scope.battleResult);
						var status = $scope.battleResult.status.toLowerCase();
						if(status.indexOf("defeat") > -1)
							knightPoints = knightPoints + 1;
						
						if(status.indexOf("victory") > -1)
							dragonPoints = dragonPoints + 1;
						
						$scope.noK = knightPoints;
						$scope.noD = dragonPoints;
					});
				});
			});       
	    });			
	}	

	function getRandomImage(type) {
		var resultURL = ""; count = type.indexOf("knights") > -1 ? 10 : 29 
		var randomNumber = Math.floor(Math.random() * count) + 1;	
		resultURL = "/images/" + type + "/" + randomNumber + ".gif";
		return resultURL;
	}
	
});



